FROM debian:buster-slim
MAINTAINER Rómulo Rodríguez rodriguezrjrr@gmail.com

# Update system and install
RUN apt-get update -y \
    && apt-get upgrade -y \
    && apt-get install -y \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg2 \
        lsb-release \
        wget \
    && curl https://packages.sury.org/php/apt.gpg | apt-key add - \
    && echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | tee /etc/apt/sources.list.d/php7.list \
    && apt-get update -y \
    && apt-get install -y \
        apache2 \
        libapache2-mod-php7.1 \
        net-tools \
        php7.1 \
        php7.1-apcu \
        php7.1-bcmath \
        php7.1-bz2 \
        php7.1-curl \
        php7.1-gd \
        php7.1-geoip \
        php7.1-imagick \
        php7.1-imap \
        php7.1-intl \
        php7.1-json \
        php7.1-ldap \
        php7.1-mcrypt \
        php7.1-mongodb \
        php7.1-msgpack \
        php7.1-mysql \
        php7.1-odbc \
        php7.1-pgsql \
        php7.1-soap \
        php7.1-sqlite \
        php7.1-sybase \
        php7.1-xsl \
        php7.1-zip \
    && apt-get autoremove -y \
    && apt-get clean -y

RUN apt-get update -y \
    && apt-get install -y \
        build-essential apache2-dev \
    && wget https://github.com/gnif/mod_rpaf/archive/v0.8.4.tar.gz \
    && tar -xzf v0.8.4.tar.gz \
    && cd mod_rpaf-0.8.4 \
    && make \
    && make install \
    && apt-get remove -y build-essential apache2-dev \
    && apt-get autoremove -y \
    && apt-get clean -y \
    && echo "LoadModule rpaf_module /usr/lib/apache2/modules/mod_rpaf.so" > /etc/apache2/mods-available/rpaf.load

ADD magic_quotes.ini /etc/php/7.1/mods-available
ADD rpaf.conf /etc/apache2/mods-available/

RUN phpenmod magic_quotes \
    && a2enmod expires \
    && a2enmod rewrite \
    && a2enmod filter \
    && a2enmod deflate \
    && a2enmod rpaf \
    && ln -sfT /dev/stderr "/var/log/apache2/error.log" \
    && ln -sfT /dev/stdout "/var/log/apache2/access.log" \
    && sed -i "s/^Listen 80\$/Listen 8080/" /etc/apache2/ports.conf

ENV PROCESSMAKER_RELEASE 3.4.5
ENV PROCESSMAKER_TARGZ "/opt/processmaker-$PROCESSMAKER_RELEASE-community.tar.gz"
RUN wget -O $PROCESSMAKER_TARGZ \
      "https://sourceforge.net/projects/processmaker/files/ProcessMaker/$PROCESSMAKER_RELEASE/processmaker-$PROCESSMAKER_RELEASE-community.tar.gz/download" \
    && mkdir /opt/processmaker

ADD entrypoint.sh /usr/bin/entrypoint.sh
ADD 000-default.conf /etc/apache2/sites-available/000-default.conf

RUN chmod -R g+w \
        /etc/php/7.1/apache2 \
        /etc/php/7.1/apache2/php.ini \
        /etc/php/7.1/cli \
        /etc/php/7.1/cli/php.ini \
        /run/apache2 \
        /var/log/apache2 \
        /opt/processmaker \
    && chmod +x /usr/bin/entrypoint.sh \
    && chown -R root:root /var/log/apache2

EXPOSE 8080
VOLUME /opt/processmaker
ENTRYPOINT ["entrypoint.sh"]
